import gitlab
import argparse
import functools

def closeRelease(app):
	release_types=['M', 'm', 'p']
	if app.release_type not in release_types:
		return 'Invalid release type'

	gl = gitlab.Gitlab(app.host_url, private_token=app.private_key)
	group = gl.groups.get(app.group_id)
	projects = group.projects.list()
	editable_projects = functools.reduce(lambda a,b:{**a,b.id:gl.projects.get(b.id,lazy=True)},projects,{})
	projects = functools.reduce(lambda a,b:{**a,b.id:b},group.projects.list(lazy=True),{})
	base_milestone = group.milestones.list(search=app.base_version, lazy=True)
	
	if len(base_milestone) == 0:
		print(f'No open milestone with {app.base_milestone}')
		return

	if len(base_milestone) > 1:
		print(f'More than one open milestone with {app.base_milestone}')
		return
	
	base_milestone = base_milestone.pop()
	print("Base Milestone", base_milestone.title)

	# Compute version names
	current_version = base_milestone.title[1:].split('.')
	next_version = [*current_version]
	position_step = release_types.index(app.release_type)
	for i in range(2,position_step,-1):
		next_version[i] = '0'
	next_version[position_step] = str(int(next_version[position_step]) + 1)
	next_version = 'v'+'.'.join(next_version)
	current_version = 'v'+'.'.join(current_version)

	# Check if have open issues
	open_issues = group.issues.list(state='opened', milestone=base_milestone.title, lazy=True)
	if not app.create_new_version and len(open_issues) >	0:
		print('Open issues, you need to move it or close before continue.')
		return

	# Checko closed Issues
	closed_issues = group.issues.list(state='closed', milestone=base_milestone.title, lazy=True, per_page=100)
	if len(closed_issues) == 0:
		print('Nothing new, no release.')
		return
	print('Closed issues', len(closed_issues))

	# Doc
	doc = []
	touched_proyects = functools.reduce(lambda a,i:[*a,i.project_id] if i.project_id not in a else a, closed_issues, [])
	closed_issues = functools.reduce(lambda a,i:{**a,i.project_id: [*a[i.project_id],i]} if i.project_id in a.keys() else {**a,i.project_id: [i]}, closed_issues, {})
	for project_id in closed_issues.keys():
		issues = closed_issues[project_id]
		project_id = int(project_id)
		project = projects[project_id]
		print('Proyect', project.name)
		doc.append(f'**[{project.name}]({project.web_url})**')
		for i in issues:
			print(i.iid, i.title)
			doc.append(f' * [#{i.iid}]({project.path}#{i.iid}): {i.title}')
			#doc.append(i.description)
			doc.append('')
	project = editable_projects[int(app.wiki_project_id)]
	try:
		existing = project.wikis.get(f'Releases/{current_version}')
		existing.delete()
	except: pass
	if not app.dry_run:
		release_wiki = project.wikis.create({'title': f'Releases/{base_milestone.title}', 'content': '  \n'.join(doc)})
		wiki_link = projects[int(app.wiki_project_id)].web_url+'/-/wikis/'+release_wiki.slug
	
	# Create release on touched projects
	for touched_proyect in touched_proyects:
		editable_project = editable_projects[touched_proyect]
		project = projects[touched_proyect]
		if app.master_branch:
			ref = editable_project.branches.list(search=app.master_branch)
			if len(ref) != 1:
				print(f'Invalid master branch filter {app.master_branch}. {len(ref)} results in {project.name}')
				return
			ref = ref[0].name
		else:
			ref = project.default_branch
		
		if not app.dry_run:
			try:
				editable_project.tags.create({'tag_name': current_version, 'ref': ref, 'message': wiki_link})
				print(f'Tag {current_version} created in {project.name}.')
			except gitlab.exceptions.GitlabCreateError as error:
				if error.response_code == 400: pass
				else: raise
		else:
			print(f'Tag {current_version} created in {project.name}.')
			

	# Create new milestone
	if app.create_new_version:
		try:
			if not app.dry_run:
				next_milestone = group.milestones.create({'title':next_version})
				print('New milestone created', next_version     )
		except gitlab.exceptions.GitlabCreateError as error:
			if error.response_code == 400:
				next_milestone =  group.milestones.list(title=next_version)
	
		# Move open issues to the new milestone
		if len(open_issues) > 0:
			print("Open Issues: Will be move it to next release")
			for open_issue in open_issues:
				editable_project = editable_projects[open_issue.project_id]
				editable_open_issue = editable_project.issues.get(open_issue.iid, lazy=True)
				print(f'{open_issue.iid} - {open_issue.title}')
				if not app.dry_run:
					editable_open_issue.milestone_id = next_milestone.id
					editable_open_issue.save()

	base_milestone.state_event = 'close'
	if not app.dry_run:
		base_milestone.save()
		project = projects[int(app.wiki_project_id)]
		print(project.web_url+'/-/wikis/'+release_wiki.slug)
		


parser = argparse.ArgumentParser(description='Release manager')
parser.add_argument('-p','--private-key', required=True, help='GitLab privete key. https://gitlab.com/profile/personal_access_tokens')
parser.add_argument("-g", "--group-id", required=True, help='Group id to versioning')
parser.add_argument("-w", "--wiki-project-id", required=True, help='Project ID where wiki is')
parser.add_argument("-b", "--base-version", required=True, help="Ej: 'v1','v1.12','v0.13.1'")
parser.add_argument("-m", "--master-branch", required=False, help="Overwiter master branch with this filter. For example 1.16$")
parser.add_argument("-n", "--create-new-version", action='store_true', help="Create new version and move open issues")
parser.add_argument("-t", "--release-type", help="M: Major, m: Minor or p: Path", default='p')
parser.add_argument("-u", "--host-url", default="https://gitlab.com",  help="Default: https://gitlab.com")
parser.add_argument("-d", "--dry-run", action='store_true', help="Run in dry mode")
args = parser.parse_args()

closeRelease(args)
