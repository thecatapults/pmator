# PMator
> Manage releases on GitLab like a boss.
 * Use Python 3.6
 * Install deps
 * Use it :rocket:
```bash
$ python createReport.py --help

usage: createReport.py [-h] -p PRIVATE_KEY -g GROUP_ID -w WIKI_PROJECT_ID -b
                       BASE_VERSION [-m MASTER_BRANCH] [-n] [-t RELEASE_TYPE]
                       [-u HOST_URL] [-d]

Release manager

optional arguments:
  -h, --help            show this help message and exit
  -p PRIVATE_KEY, --private-key PRIVATE_KEY
                        GitLab privete key. https://gitlab.com/pr
                        ofile/personal_access_tokens
  -g GROUP_ID, --group-id GROUP_ID
                        Group id to versioning
  -w WIKI_PROJECT_ID, --wiki-project-id WIKI_PROJECT_ID
                        Project ID where wiki is
  -b BASE_VERSION, --base-version BASE_VERSION
                        Ej: 'v1','v1.12','v0.13.1'
  -m MASTER_BRANCH, --master-branch MASTER_BRANCH
                        Overwiter master branch with this filter. For example
                        1.16$
  -n, --create-new-version
                        Create new version and move open issues
  -t RELEASE_TYPE, --release-type RELEASE_TYPE
                        M: Major, m: Minor or p: Path
  -u HOST_URL, --host-url HOST_URL
                        Default: https://gitlab.com
  -d, --dry-run         Run in dry mode
```
